import {
  Button,
  Card,
  Classes,
  ControlGroup,
  HTMLSelect,
} from '@blueprintjs/core';
import * as React from 'react';
import {
  FilterItem as FilterItemType,
  FilterOperation,
  useFilterState,
} from '../contexts/FilterContext';
import { ExperimentParameters } from '../types/data';

export interface FiltersProps {
  experimentParameters: ExperimentParameters;
}
export const Filters = ({ experimentParameters }: FiltersProps) => {
  const [state, actions] = useFilterState();
  const filters =
    state.filters.length > 0
      ? state.filters
      : [{ operation: 'equal' } as FilterItemType];
  return (
    <Card>
      {filters.map((filter, index) => (
        <FilterItem
          key={index}
          index={index}
          experimentParameters={experimentParameters}
          {...filter}
        />
      ))}
      <div>
        <Button
          icon="add"
          onClick={() => actions.add({ operation: 'equal', value: 0 })}
          intent="success"
        >
          Add Filter
        </Button>
      </div>
    </Card>
  );
};

export interface FilterItemProps extends FilterItemType {
  experimentParameters: ExperimentParameters;
  index: number;
}

const FilterItem = ({
  experimentParameters,
  value,
  operation,
  parameter,
  index,
}: FilterItemProps) => {
  const [, action] = useFilterState();

  return (
    <div>
      <ControlGroup vertical={false} className="gc-filters-wrapper">
        <span className="gc-filters-item">
          <HTMLSelect
            value={parameter}
            options={[
              'Select Paremeter',
              ...experimentParameters.inputs,
              ...experimentParameters.outputs,
            ]}
            onChange={(e) =>
              action.update(index, {
                parameter: e.currentTarget.value,
              })
            }
          />
        </span>
        <span className="gc-filters-item">
          <HTMLSelect
            value={operation}
            options={['equal', 'less', 'more']}
            onChange={(e) =>
              action.update(index, {
                operation: e.currentTarget.value as FilterOperation,
              })
            }
          />
        </span>
        <span className="gc-filters-item">
          <input
            type="number"
            className={Classes.INPUT}
            value={value}
            onChange={(e) =>
              action.update(index, {
                value: isNaN(parseFloat(e.currentTarget.value))
                  ? undefined
                  : parseFloat(e.currentTarget.value),
              })
            }
          />
        </span>

        <Button
          icon="delete"
          intent="danger"
          onClick={() => action.remove(index)}
        ></Button>
      </ControlGroup>
    </div>
  );
};
