import {
  Alignment,
  Classes,
  Navbar,
  NavbarGroup,
  NavbarHeading,
} from '@blueprintjs/core';

export const Header = () => {
  return (
    <Navbar className={Classes.DARK}>
      <NavbarGroup align={Alignment.LEFT}>
        <NavbarHeading>Awesome Graph</NavbarHeading>
      </NavbarGroup>
    </Navbar>
  );
};
