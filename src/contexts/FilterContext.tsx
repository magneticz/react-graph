import * as React from 'react';
import { Experiment, ExperimentParams } from '../types/data';

type Action =
  | { type: 'add'; item: FilterItem }
  | { type: 'remove'; index: number }
  | { type: 'update'; item: Partial<FilterItem>; index: number }
  | { type: 'reset' };

type Dispatch = (action: Action) => void;

export type FilterOperation = 'equal' | 'less' | 'more';

export interface FilterItem {
  parameter?: string;
  value?: number;
  operation: FilterOperation;
}
export interface FilterContextState {
  filters: Array<FilterItem>;
}

type FilterProviderProps = {
  children: React.ReactNode;
  value?: FilterContextState;
};

const defaultFilterContextState: FilterContextState = {
  filters: [],
};

function filterReducer(
  state: FilterContextState,
  action: Action
): FilterContextState {
  switch (action.type) {
    case 'add': {
      return {
        ...state,
        filters: [...state.filters, action.item],
      };
    }
    case 'update': {
      const newFilters = [...state.filters];
      const oldItem = newFilters[action.index];
      newFilters[action.index] = { ...oldItem, ...action.item };
      return { ...state, filters: newFilters };
    }
    case 'remove': {
      const newFilters = [...state.filters];
      newFilters.splice(action.index, 1);
      console.log(newFilters);
      return { ...state, filters: newFilters };
    }
    case 'reset': {
      return { ...defaultFilterContextState };
    }
  }
}

const FilterStateContext = React.createContext<FilterContextState | undefined>(
  undefined
);
const FilterDispatchContext = React.createContext<Dispatch | undefined>(
  undefined
);

export function FilterProvider({
  children,
  value = defaultFilterContextState,
}: FilterProviderProps) {
  const [state, dispatch] = React.useReducer(filterReducer, value);

  return (
    <FilterStateContext.Provider value={state}>
      <FilterDispatchContext.Provider value={dispatch}>
        {children}
      </FilterDispatchContext.Provider>
    </FilterStateContext.Provider>
  );
}

export function useFilterState() {
  const state = React.useContext(FilterStateContext);
  if (state === undefined) {
    throw new Error('useFilterState must be used within a FilterProvider');
  }
  const dispatch = React.useContext(FilterDispatchContext);
  if (dispatch === undefined) {
    throw new Error(
      'FilterDispatchContext must be used within a FilterProvider'
    );
  }
  const actions = {
    add(item: FilterItem) {
      dispatch({ type: 'add', item });
    },
    remove(index: number) {
      console.log(index);
      dispatch({ type: 'remove', index });
    },
    update(index: number, item: Partial<FilterItem>) {
      dispatch({ type: 'update', index, item });
    },
    reset() {
      dispatch({ type: 'reset' });
    },
  };
  return [state, actions] as const;
}

export const filterData = (
  experiments: Array<Experiment>,
  filters: Array<FilterItem>
) => {
  return experiments.filter((exp) => {
    return filters.every((item) => {
      if (!item.value || !item.parameter) {
        return true;
      }
      const param = item.parameter as ExperimentParams;
      switch (item.operation) {
        case 'equal':
          return item.value === exp.allParams[param];
        case 'less':
          return item.value > exp.allParams[param];
        case 'more':
          return item.value < exp.allParams[param];
        default:
          return false;
      }
    });
  });
};
