import * as React from 'react';
import { H3, H5, HTMLSelect, Card } from '@blueprintjs/core';
import { ExperimentParameters, ExperimentParams } from '../types/data';

export interface GraphControlOptions {
  activeExperiment?: string;
  xAxis: ExperimentParams;
  yAxis: ExperimentParams;
  size?: ExperimentParams;
  color?: ExperimentParams;
}

export interface GraphControlsProps {
  enabledControls: Array<string>;
  selectedControls: GraphControlOptions;
  onControlSelect(control: GraphControlOptions): void;
  experimentParameters: ExperimentParameters;
}

export const GraphControls = ({
  enabledControls,
  onControlSelect,
  selectedControls,
  experimentParameters,
  children,
}: React.PropsWithChildren<GraphControlsProps>) => {
  return (
    <Card>
      <H3>Graph Controls</H3>
      <Card>
        <div className="gc-display-row">
          <div className="gc-graph-controls-control">
            <H5>Highlight Experiment</H5>
            <HTMLSelect
              value={selectedControls.activeExperiment || 'Select experiment'}
              options={[
                'Select experiment',
                ...experimentParameters.experimentIds,
              ]}
              onChange={(e) =>
                onControlSelect({
                  ...selectedControls,
                  activeExperiment: e.currentTarget.value,
                })
              }
            />
          </div>
          <div className="gc-graph-controls-control">
            <H5>Select Parameter for X Axis</H5>
            <HTMLSelect
              value={selectedControls.xAxis}
              disabled={enabledControls.indexOf('xAxis') < 0}
              options={[
                ...experimentParameters.inputs,
                ...experimentParameters.outputs,
              ]}
              onChange={(e) =>
                onControlSelect({
                  ...selectedControls,
                  xAxis: e.currentTarget.value as ExperimentParams,
                })
              }
            />
          </div>
          <div className="gc-graph-controls-control">
            <H5>Select Parameter for Y Axis</H5>
            <HTMLSelect
              value={selectedControls.yAxis}
              disabled={enabledControls.indexOf('yAxis') < 0}
              options={[
                ...experimentParameters.inputs,
                ...experimentParameters.outputs,
              ]}
              onChange={(e) =>
                onControlSelect({
                  ...selectedControls,
                  yAxis: e.currentTarget.value as ExperimentParams,
                })
              }
            />
          </div>
          <div className="gc-graph-controls-control">
            <H5>Select Parameter for Size</H5>
            <HTMLSelect
              value={selectedControls.size}
              disabled={enabledControls.indexOf('size') < 0}
              options={[
                'Select',
                ...experimentParameters.inputs,
                ...experimentParameters.outputs,
              ]}
              onChange={(e) =>
                onControlSelect({
                  ...selectedControls,
                  size:
                    e.currentTarget.value !== 'Select'
                      ? (e.currentTarget.value as ExperimentParams)
                      : undefined,
                })
              }
            />
          </div>
          {children}
        </div>
      </Card>
    </Card>
  );
};
