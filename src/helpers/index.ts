import { Experiment, ExperimentParams } from "../types/data";

export const getMinMax = (
  experiments: Array<Experiment>,
  k: ExperimentParams
): [number, number] => {
  let min = Infinity;
  let max = -Infinity;
  for (let i = 0; i < experiments.length; i++) {
    const { allParams } = experiments[i];
    if (min > allParams[k]) min = allParams[k];
    if (max < allParams[k]) max = allParams[k];
  }

  return [min, max];
};
