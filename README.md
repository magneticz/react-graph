# React Graph

This is the implementation of coding case for Uncountable. 
The app is implemented with React as a Frontend Library, Nivo as a visualization library and Blueprint as a component library.

## How to run
This project can be run in two ways. Below are quick guides on how to do it. Before proceeding, please clone the repository

```
git clone git@bitbucket.org:magneticz/react-graph.git
cd react-graph
```
### With Docker
You can run the app by building docker image. To build it, please run

```
docker build -t react-graph .
```

After docker image is built, you can start it with 
```
docker run -it -p 5000:5000 react-graph
```

### With React scripts
This is the simpler option, if you already have node installed. You just need to install dependencies, and run the application.

then depending on which package manager you use, you can run
```
yarn install
```
or
```
npm install
```

After installation is complete you can run
```
yarn start
```

NOTE: This option will run the app in development mode, while for this particular application it doesn't really matter, if you want to run in production mode, you need to separately install some other tool that will serve static files. Package `serve` is a good candidate.

To run with `serve`, you can do:

```bash
yarn build # build the app
yarn add serve # install the serve package, can be installed globally with -g key
serve -s build # serve static files
```