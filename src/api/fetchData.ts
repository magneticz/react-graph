import data from '../data.json';

export function fetchData<T>(): PromiseLike<T> {
  return Promise.resolve((data as unknown) as T);
}
