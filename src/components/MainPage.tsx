import {
  Button,
  Classes,
  Drawer,
  H5,
  Radio,
  RadioGroup,
} from '@blueprintjs/core';
import * as React from 'react';
import { fetchData } from '../api/fetchData';
import { filterData, useFilterState } from '../contexts/FilterContext';
import {
  Experiment,
  ExperimentParameters,
  ExperimentsRawData,
} from '../types/data';
import { ExperimentDescription } from './ExperimentDescription';
import { Filters } from './Filters';
import { GraphControlOptions, GraphControls } from './GraphControls';
import { ScatterPlot, BarChart } from './Graphs';

const getEnabledControls = (graphType: string, activeExp?: string) => {
  const allControls = ['xAxis', 'yAxis', 'size'];
  if (graphType === 'scatterplot') {
    return allControls;
  }
  if (graphType === 'barchart') {
    return activeExp === undefined ? ['size'] : [];
  }
  return allControls;
};

export const MainPage = () => {
  const [graphControls, setGraphControls] = React.useState<GraphControlOptions>(
    {
      xAxis: 'Plasticizer 1',
      yAxis: 'Tensile Strength',
    }
  );
  const [allExperiments, setExperiments] = React.useState<Array<Experiment>>(
    []
  );
  const [filters] = useFilterState();
  const filteredExperiments = filterData(allExperiments, filters.filters);
  const [
    experimentParameters,
    setExperimentParameters,
  ] = React.useState<ExperimentParameters>({
    inputs: [],
    outputs: [],
    experimentIds: [],
  });

  const [showFilters, setShowFilters] = React.useState(false);
  const [selectedGraph, setSelectedGraph] = React.useState<string>(
    'scatterplot'
  );
  React.useEffect(() => {
    fetchData<ExperimentsRawData>().then((data) => {
      const experimentIds = Object.keys(data);
      const parsedData = experimentIds.map((key) => ({
        id: key,
        allParams: { ...data[key].inputs, ...data[key].outputs },
        ...data[key],
      }));

      setExperiments(parsedData);
      setExperimentParameters({
        inputs: Object.keys(parsedData[0].inputs),
        outputs: Object.keys(parsedData[0].outputs),
        experimentIds,
      });
    });
  }, []);
  const enabledControls = getEnabledControls(
    selectedGraph,
    graphControls.activeExperiment
  );
  console.log(enabledControls);
  return (
    <div>
      <GraphControls
        enabledControls={enabledControls}
        selectedControls={graphControls}
        onControlSelect={setGraphControls}
        experimentParameters={experimentParameters}
      >
        <div className="gc-graph-controls-control">
          <H5>Choose Graph type</H5>
          <RadioGroup
            onChange={(e) => setSelectedGraph(e.currentTarget.value)}
            selectedValue={selectedGraph}
          >
            <Radio label="Bar Chart" value="barchart" />
            <Radio label="Scatter Plot" value="scatterplot" />
          </RadioGroup>
        </div>
        <div className="gc-graph-controls-control">
          <H5>Open Filters Panel</H5>
          <Button onClick={() => setShowFilters(true)}>
            Show Filters{' '}
            {filters.filters.length > 0 && (
              <span>({filters.filters.length})</span>
            )}
          </Button>
        </div>
      </GraphControls>
      <div>
        <div className="gc-display-row">
          <div className="gc-display-column">
            {selectedGraph === 'scatterplot' ? (
              <ScatterPlot
                data={filteredExperiments}
                options={graphControls}
                onNodeSelect={(id) =>
                  setGraphControls({ ...graphControls, activeExperiment: id })
                }
              />
            ) : (
              <BarChart
                data={filteredExperiments}
                options={graphControls}
                onNodeSelect={(id) =>
                  setGraphControls({ ...graphControls, activeExperiment: id })
                }
              />
            )}
          </div>
          {graphControls.activeExperiment && (
            <ExperimentDescription
              onClose={() =>
                setGraphControls({
                  ...graphControls,
                  activeExperiment: undefined,
                })
              }
              experiment={
                allExperiments.filter(
                  (e) => e.id === graphControls.activeExperiment
                )[0]
              }
            />
          )}
        </div>
      </div>

      <Drawer
        isOpen={showFilters}
        onClose={() => setShowFilters(false)}
        icon="filter"
        title="Filter Experiments based on Parameters"
      >
        <div className={Classes.DRAWER_BODY}>
          <Filters experimentParameters={experimentParameters} />
        </div>
      </Drawer>
    </div>
  );
};
