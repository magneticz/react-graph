FROM node:lts-alpine

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY . ./
RUN yarn install --silent
RUN yarn add serve

RUN yarn build

CMD [ "serve", "-n", "build" ]