import * as React from 'react';
import {
  ScatterPlot as ScatterPlotNivo,
  ScatterPlotProps as ScatterPlotNivoProps,
} from '@nivo/scatterplot';
import { Dimensions } from '@nivo/core';
import { Experiment, ExperimentParams } from '../../types/data';
import { GraphControlOptions } from '../GraphControls';
import { getMinMax } from '../../helpers';

const commonProps: ScatterPlotNivoProps & Dimensions = {
  width: 900,
  height: 500,
  margin: { top: 24, right: 24, bottom: 80, left: 80 },
  nodeSize: 10,
  blendMode: 'multiply',
  data: [],
  legends: [
    {
      anchor: 'bottom-left',
      direction: 'row',
      translateY: 60,
      itemWidth: 130,
      itemHeight: 12,
      symbolSize: 12,
      symbolShape: 'circle',
    },
  ],
};
export const scatterPlotDataPrep = (
  data: Array<Experiment>,
  x: ExperimentParams,
  y: ExperimentParams,
  z?: ExperimentParams
) => {
  return data.map((e) => {
    return {
      id: e.id,
      x: e.allParams[x],
      y: e.allParams[y],
      z: z && e.allParams[z],
    };
  });
};

export interface ScatterPlotProps {
  onNodeSelect(id: string): void;
  options: GraphControlOptions;
  data: Array<Experiment>;
}

export const ScatterPlot = ({
  data,
  onNodeSelect,
  options,
}: ScatterPlotProps) => {
  const { active: experiment, rest } = data.reduce<{
    rest: Array<Experiment>;
    active?: Experiment;
  }>(
    (acc, curr) => {
      if (curr.id === options.activeExperiment) {
        return { ...acc, active: curr };
      }
      return { ...acc, rest: [...acc.rest, curr] };
    },
    { rest: [] }
  );

  const computed = [
    {
      id: 'All Experiments',
      data: scatterPlotDataPrep(
        rest,
        options.xAxis,
        options.yAxis,
        options.size
      ),
    },
  ];
  if (experiment) {
    computed.push({
      id: 'Selected Experiment',
      data: scatterPlotDataPrep(
        [experiment],
        options.xAxis,
        options.yAxis,
        options.size
      ),
    });
  } else {
  }
  const nodeSize = options.size && getMinMax(data, options.size);
  return (
    <ScatterPlotNivo
      {...commonProps}
      data={computed}
      nodeSize={nodeSize && { key: 'z', sizes: [9, 45], values: nodeSize }}
      colors={{ scheme: 'dark2' }}
      onClick={(node) => {
        if (computed[0].data[node.index]) {
          onNodeSelect(computed[0].data[node.index].id);
        }
      }}
    />
  );
};
