import * as React from 'react';
import { Bar } from '@nivo/bar';
import { GraphControlOptions } from '../GraphControls';
import { Experiment, ExperimentParams } from '../../types/data';

const commonProps = {
  width: 900,
  height: 500,
  margin: { top: 60, right: 80, bottom: 60, left: 80 },
  indexBy: 'country',
  padding: 0.2,
  labelTextColor: 'inherit:darker(1.4)',
  labelSkipWidth: 16,
  labelSkipHeight: 16,
};

const barchartDataPrep = (
  data: Array<Experiment>,
  options: GraphControlOptions
) => {
  let computed;
  if (options.activeExperiment) {
    const activeExp = data.filter(
      (exp) => exp.id === options.activeExperiment
    )[0];
    computed = Object.keys(activeExp.allParams).map((key) => {
      return {
        id: key,
        value: activeExp.allParams[key as ExperimentParams],
      };
    });
    return { computed, keys: ['value'] };
  }

  computed = data.map((exp) => ({ id: exp.id, ...exp.allParams }));

  return {
    computed,
    keys: options.size ? [options.size] : Object.keys(data[0].outputs),
  };
};
export interface BarChartProps {
  onNodeSelect(id: string): void;
  options: GraphControlOptions;
  data: Array<Experiment>;
}
export const BarChart = ({ data, onNodeSelect, options }: BarChartProps) => {
  const { computed, keys } = barchartDataPrep(data, options);

  return (
    <Bar
      {...commonProps}
      data={computed}
      keys={keys}
      groupMode="stacked"
      indexBy="id"
      valueScale={{ type: 'symlog' }}
      axisBottom={{
        tickValues: 3,
        tickRotation: -20,
        legendOffset: -80,
        legendPosition: 'start',
      }}
      onClick={(e) => {
        onNodeSelect(e.indexValue as string);
      }}
    />
  );
};
