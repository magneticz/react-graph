import { Button, Card, H5 } from '@blueprintjs/core';
import * as React from 'react';
import { Experiment, Inputs, Outputs } from '../types/data';

export interface ExperimentDescriptionProps {
  experiment: Experiment;
  onClose(): void;
}
export const ExperimentDescription = ({
  experiment,
  onClose,
}: ExperimentDescriptionProps) => {
  return (
    <div className="bp3-dialog gc-dialog">
      <div className="bp3-dialog-header gc-dialog-header">
        <span className="bp3-icon-large bp3-icon-lab"></span>
        <h4 className="bp3-heading">Experiment {experiment.id}</h4>
        <button
          aria-label="Close"
          className="bp3-dialog-close-button bp3-button bp3-minimal bp3-icon-cross"
          onClick={onClose}
        ></button>
      </div>
      <div className="bp3-dialog-body">
        <Card>
          <H5>Inputs</H5>
          {Object.keys(experiment.inputs).map((key) => {
            return (
              <div>
                {key}: {experiment.inputs[key as keyof Inputs]}
              </div>
            );
          })}
        </Card>
        <Card>
          <H5>Outputs</H5>
          {Object.keys(experiment.outputs).map((key) => {
            return (
              <div>
                {key}: {experiment.outputs[key as keyof Outputs]}
              </div>
            );
          })}
        </Card>
      </div>
      <div className="bp3-dialog-footer">
        <div className="bp3-dialog-footer-actions">
          <Button onClick={onClose} intent="primary">
            Close
          </Button>
        </div>
      </div>
    </div>
  );
};
