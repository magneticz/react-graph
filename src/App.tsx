import React from 'react';
import { MainPage } from './components/MainPage';
import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import './app.css';

import { Header } from './layout/Header';
import { FilterProvider } from './contexts/FilterContext';

function App() {
  return (
    <div>
      <FilterProvider>
        <Header />
        <MainPage />
      </FilterProvider>
    </div>
  );
}

export default App;
